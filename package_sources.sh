#!/bin/bash

VERSION=70.0.6

pushd sources
tar cvJf ../deskos-logos-${VERSION}.tar.xz --dereference centos-logos-${VERSION}
popd
